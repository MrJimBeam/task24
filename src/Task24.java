import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task24 {
    public static void main(String[] args) throws FileNotFoundException {
        //lists for adding opening and closing parentheses
        List<Character> charList = new ArrayList<>();
        List<Character> charListRight = new ArrayList<>();
        if (args.length < 1) {
            System.out.println("Please provide java file name in the argument");
            System.exit(1);
        }
        //reads file
        File file = new File(args[0]);
        if (!file.exists()) {
            System.out.println(args[0]);
            System.out.println("The file does not exists, please enter valid filename");
        } else {
            Scanner mysC = new Scanner(file);
            while (mysC.hasNext()) {
                String word = mysC.next();

                for (int i = 0; i < word.length(); i++) {
                    //handles running with this file itself and the parentheses in the if-tests
                    if (word.length() > 2 && (i + 1) < word.length() && word.charAt(i + 1) == '\'') {
                    } else {
                        //adds all opening parentheses to list
                        if (word.charAt(i) == '{' || word.charAt(i) == '(' || word.charAt(i) == '[') {
                            charList.add(word.charAt(i));
                        }
                        //add all closing ones for error-checking
                        if (word.charAt(i) == ')' || word.charAt(i) == '}' || word.charAt(i) == ']') {
                            charListRight.add(word.charAt(i));
                        }
                        //three tests to check if the closing parenthesis matches the last opened.
                        if (!charList.isEmpty() && charList.get(charList.size() - 1) == '{' && word.charAt(i) == '}') {
                            charList.remove(charList.size() - 1);
                            charListRight.remove(charListRight.size() - 1);
                        }
                        if (!charList.isEmpty() && charList.get(charList.size() - 1) == '(' && word.charAt(i) == ')') {
                            charList.remove(charList.size() - 1);
                            charListRight.remove(charListRight.size() - 1);
                        }
                        if (!charList.isEmpty() && charList.get(charList.size() - 1) == '[' && word.charAt(i) == ']') {
                            charList.remove(charList.size() - 1);
                            charListRight.remove(charListRight.size() - 1);
                        }
                    }
                }
            }
        }
        if(charList.size() == 0 && charListRight.size() == 0){
            System.out.println("Congrats, you are a pro at java-syntax!");
        } else {
            System.out.println("Noob, you failed to match your parentheses");
            //prints potential unmatched parentheses
            System.out.println("Unmatched parentheses: ");
            for (Character a : charList) {
                System.out.print(a + " ");
            }
            System.out.print(" -> ");
            for (Character a : charListRight) {
                System.out.print(a + " ");
            }
        }
    }
}
